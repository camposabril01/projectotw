using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciaPickupItems : MonoBehaviour
{
    
    private float TiempoDespawn = 10f;
    [SerializeField]private float TiempoDespawnTrascurrido = 10f;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Soy un item y me recogiste");
        Destroy(this.gameObject);
        
    }
    private void Update()
    {
        TiempoDespawnTrascurrido -= Time.deltaTime;
        if(TiempoDespawnTrascurrido <= 0)
        {
            Destroy(this.gameObject);
            TiempoDespawnTrascurrido = TiempoDespawn;
        }
    
    }
}
