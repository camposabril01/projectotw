using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciarItems : MonoBehaviour
{
    [SerializeField] private GameObject TestItem;
    [SerializeField] float TiempoDeSpawn = 20f;
    [SerializeField] float TiempoTranscurridoSpawn;

    // Start is called before the first frame update
    void Start()
    {
        TiempoTranscurridoSpawn = TiempoDeSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        TimerSpawn();
    }
    private void SpawnItem()
    {
        Instantiate(TestItem, transform.position, Quaternion.identity);   
    }

    private void TimerSpawn()
    {
        TiempoTranscurridoSpawn -= Time.deltaTime;
        if (TiempoTranscurridoSpawn <= 0)
        {
            SpawnItem();
            TiempoTranscurridoSpawn = TiempoDeSpawn;
        }
    }
}
