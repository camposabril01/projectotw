using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    // Esta variable resetea stats y algunas cosas como el timer a su valor inicial al iniciar la partida 
    // O, alternativamente, en ciertos casos a valores menores. 
    public void ComenzarPartida()
    {
        TimerDiaController.instancia.IniciarDia();
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
