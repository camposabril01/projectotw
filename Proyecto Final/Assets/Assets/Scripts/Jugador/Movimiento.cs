using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    //Movimiento
    private float movimientoHorizontal, movimientoVertical;
    [SerializeField] private float velocidadMovimiento;
    [SerializeField] private float velocidadRotacion = 150f;
    private Animator anim;
    //Estamina afectando la velocidad
    public MecanicaStamina mecanicaStamina;
    public StatsJugador statsJugador;
    public GameObject jugador;
    private RaycastActividades raycastActividades;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        mecanicaStamina = jugador.GetComponent<MecanicaStamina>();
        raycastActividades = jugador.GetComponent<RaycastActividades>();
    }

    // Update is called once per frame
    void Update()
    {
            
        MovimientoPersonaje();
        HelperControlStamina();
        
    }
    private void MovimientoPersonaje()
    {
        //sector mover
        movimientoHorizontal = Input.GetAxis("Horizontal");
        movimientoVertical = Input.GetAxis("Vertical");

        transform.Rotate(0, movimientoHorizontal * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(movimientoHorizontal * Time.deltaTime * velocidadMovimiento,0,movimientoVertical * Time.deltaTime * velocidadMovimiento);
        
        if (Input.GetKeyDown(KeyCode.E))
        {
            transform.Rotate(0, 180, 0);
        }
        anim.SetFloat("VelocidadH",movimientoHorizontal);
        anim.SetFloat("VelocidadV", movimientoVertical);
    }

    private void HelperControlStamina()
    {
        int staminaActual = jugador.GetComponent<StatsJugador>().estaminaActual;
        mecanicaStamina.DefinirNivel(staminaActual);
        velocidadMovimiento = jugador.GetComponent<MecanicaStamina>().vel;
    }

    
}
