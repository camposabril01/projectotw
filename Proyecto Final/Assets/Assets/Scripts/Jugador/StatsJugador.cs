using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsJugador : MonoBehaviour
{
    public int estaminaDefault = 100;
    public int estaminaActual;
    public BarraDeStamina barraStamina;
    
    void Start()
    {
        //Reset de stamina al darle play
        estaminaActual = estaminaDefault;
        barraStamina.CantidadDeStaminaMax(estaminaDefault);
    }

    void Update()
    {
        //Borrar luego, solo debug
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RestarStamina(10);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SumarStamina(5);
        }

    }
    //Borrar luego, solo debug
    public void SumarStamina(int suma)
    {
        estaminaActual += suma;
        barraStamina.CantidadDeStamina(estaminaActual);
    }
    public void RestarStamina(int resta)
    {
        estaminaActual -= resta;
        barraStamina.CantidadDeStamina(estaminaActual);
    }
}
