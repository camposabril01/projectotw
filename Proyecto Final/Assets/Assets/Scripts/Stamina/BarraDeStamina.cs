using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeStamina : MonoBehaviour
{
    //Script para controlar la stamina en la UI
    public Slider BarraStamina;
    public Gradient gradiente;
    public Image relleno;
    public void CantidadDeStamina(int stamina)
    {
        //Sube o baja el nivel de la barra de stamina visualmente
        BarraStamina.value = stamina;
        relleno.color = gradiente.Evaluate(BarraStamina.normalizedValue);
        
    }

    public void CantidadDeStaminaMax(int staminamax)
    {
        //Setea la barra de stamina al nivel maximo actual
        BarraStamina.maxValue = staminamax;
        BarraStamina.value = staminamax;

        relleno.color = gradiente.Evaluate(1f);
    }
}
