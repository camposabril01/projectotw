using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MecanicaStamina : MonoBehaviour
{
    public enum EstadoStamina
    {
        Alto,
        Medio,
        Bajo
    }
    public EstadoStamina estadoActual;
    public float vel;
    public void DefinirNivel(int stamina)
    {
        if (stamina <= 25)
        {
            estadoActual = EstadoStamina.Bajo;
        }
        if (stamina >= 56)
        {
            estadoActual = EstadoStamina.Alto;
        }
        if (stamina <= 55 && stamina >= 26)
        {
            estadoActual = EstadoStamina.Medio;
        }
        AfectarVelocidad(estadoActual);
    }

    public void AfectarVelocidad(EstadoStamina estadoActual)
    {
        switch (estadoActual)
        {
            case EstadoStamina.Alto:
                vel = 4.5f;
                break;
            case EstadoStamina.Medio:
                vel = 4f;
                break;
            case EstadoStamina.Bajo:
                vel = 3.25f;
                break;
        }
    }
}
