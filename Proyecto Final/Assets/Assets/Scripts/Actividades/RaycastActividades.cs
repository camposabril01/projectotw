using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastActividades : MonoBehaviour
{
    [SerializeField] private float distanciaMax = 1.5f;
    [SerializeField] private LayerMask layerDeActividad;
    [SerializeField] private GameObject jugador;

   

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Raycast();
        }
        
    }

    
    private void Raycast()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position, transform.TransformDirection(Vector3.forward));

        if (Physics.Raycast(ray, out hit, distanciaMax, layerDeActividad))
        {
            Debug.Log("Estoy funcionando");
            if (hit.transform.GetComponent<MecanicaActividades>() == true)
            {
                Debug.Log("Sigo funcionando");
                hit.transform.GetComponent<MecanicaActividades>().DefinirActividad();
                hit.transform.GetComponent<MecanicaActividades>().DefinirPesoActividad();
            }
        }
    }
}
