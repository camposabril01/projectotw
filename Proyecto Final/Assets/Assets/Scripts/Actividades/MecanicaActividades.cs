using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MecanicaActividades : MonoBehaviour
{
    public enum PesoActividad
    {
        Alto,
        Medio,
        Bajo
    }

    public enum TipoActividad
    {
        Quehacer,
        Esparcimiento,
        Descanso
    }
    public TipoActividad tipoActividad;
    public PesoActividad pesoActividad;
    public int cantidadStamina;
    public int quitarStamina;
    public int sumarStamina;
    [SerializeField] private StatsJugador statsJugador;
    [SerializeField] private MecanicaStamina mecanicaStamina;
    [SerializeField] private GameObject jugador;

    private void Start()
    {
        statsJugador = jugador.GetComponent<StatsJugador>();
        mecanicaStamina = jugador.GetComponent<MecanicaStamina>();
        //v No se si esto sirve o no, pero antes de que esto sea testeable no toco nada v
        MecanicaStamina.EstadoStamina estadoStaminaActual = mecanicaStamina.estadoActual;
        
    }

    public void DefinirActividad()
    {
        switch (tipoActividad)
        {
            case TipoActividad.Quehacer:
                RestaStamina();
                break;
            case TipoActividad.Esparcimiento:
                SumaStamina();
                break;
            case TipoActividad.Descanso:
                SumaStamina();
                break;
            default:
                break;
        }
    }
    public void DefinirPesoActividad()
    {
        switch (pesoActividad)
        {
            case PesoActividad.Alto:
                cantidadStamina = 5;
                break;
            case PesoActividad.Medio:
                cantidadStamina = 3;
                break;
            case PesoActividad.Bajo:
                cantidadStamina = 1;
                break;
            default:
                break;
        }
    }
    
    public void SumaStamina()
    {
        //v si se rompe lloro, pudo haber sido un if antes del switch pero problemas tecnicos. problema de abril refactorizando. v
        switch (tipoActividad)
        {
            case TipoActividad.Esparcimiento:
                switch (mecanicaStamina.estadoActual)
                {
                    case MecanicaStamina.EstadoStamina.Alto:
                        cantidadStamina -= 1;
                        statsJugador.SumarStamina(cantidadStamina);
                        break;
                    case MecanicaStamina.EstadoStamina.Medio:
                        statsJugador.SumarStamina(cantidadStamina);
                        break;
                    case MecanicaStamina.EstadoStamina.Bajo:
                        cantidadStamina -= cantidadStamina - 1;
                        break;
                    default:
                        break;
                }
                break;
            case TipoActividad.Descanso:
                switch (mecanicaStamina.estadoActual)
                {
                    case MecanicaStamina.EstadoStamina.Alto:
                        cantidadStamina = 0; //No se supone que se pueda descansar con stamina alto,
                                             //aun no tengo la herramienta para implementar esto.
                                             //quitar luego. Si, es redundante.
                        break;
                    case MecanicaStamina.EstadoStamina.Medio:
                        statsJugador.SumarStamina(cantidadStamina);
                        break;
                    case MecanicaStamina.EstadoStamina.Bajo:
                        cantidadStamina *= 2;
                        statsJugador.SumarStamina(cantidadStamina);
                        break;

                }
                break;
        }
    }

    public void RestaStamina()
    {
        statsJugador.RestarStamina(cantidadStamina);
    }
}
