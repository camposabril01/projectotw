using System; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDiaController : MonoBehaviour
{
    public static TimerDiaController instancia;

    //NF: Nota Futuro, cosas que modificar o info de que es algo

    //Canvas
    public Text contadorTiempo;
    //Script
    private TimeSpan tiempoJugando;
    private float tiempoTranscurrido;
    private bool timerAvanzando;
    
    //Se autoinstancia
    private void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //Setea todo de vuelta a su valor original
    void Start()
    {
        contadorTiempo.text = "Tiempo: 00:00";
        timerAvanzando = false;
    }

    //Conexi�n con: GameManager.
    public void IniciarDia()
    {
        timerAvanzando = true;
        tiempoTranscurrido = 0f;

        StartCoroutine(CircularTiempo());
    }
    
    //NF1 - Esto es una corutina y se ejecuta al mismo tiempo que otras, en
    //el mismo thread de procesador.
    //NF2 - Limitar el tiempo a 15 minutos luego para iniciar un nuevo dia,
    //puede que se tenga que
    //modificar el script considerablemente.
    private IEnumerator CircularTiempo()
    {
        while (timerAvanzando)
        {
            tiempoTranscurrido += Time.deltaTime;
            tiempoJugando = TimeSpan.FromSeconds(tiempoTranscurrido);
            string tiempoJugandoStr = "Tiempo: " + tiempoJugando.ToString("mm':'ss");
            contadorTiempo.text = tiempoJugandoStr;

            yield return null;
        }
    }

    // Update is called once per frame

    public void FinDelDia()
    {
        timerAvanzando = false;
    }
    
}
