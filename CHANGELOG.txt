Actualización 1 - 0.0.1
Se agrega personaje y movimiento. Se establece una barra de stamina en la UI solamente.

Bug: La barra de stamina se buggea graficamente y no se todavia como arreglarlo

Actualización 2- 0.0.2
Fix temporal de bug anterior: Barra de stamina con imagenes default de Unity, hasta que sepa arreglarlo. Se agrega funcionalidad a la barra de cambiar de color según el porcentaje de vida. 
Q para subir 5 de stamina, Espacio para bajar 10. (quitar luego)
Agregué una escena nueva para ayudarme con la creación de aspectos de UI.

Reutilizar mas adelante el script para cambiar el color de la barra y que se vaya achicando o alargando cuando sea hora de hacer los timers de Actividades.
Bug?: Si se cambia de Full HD(1920x1080) a Free Aspect en el visor de Game la barra se ve gigante, pero ahora por lo menos no se rompe.

Actualización 3- 0.0.3
Todo el día, ¡pero la stamina ahora modifica la velocidad! Tome la decision ejecutiva de cambiar los porcentajes a valores fijos. Es mas facil para todos.
BUG: La barra de stamina no necesariamente refleja el estado que se encuentra la stamina al cambiar de color, en ciertos valores.

Actualización 4- 0.0.4
Script de Actividad hecho, falta implementar para utilización.
Sin bugs nuevos observados.

Actualización 5- 0.0.5 - 5/2/22 14:00 
Script de Timer hecho y colocado en la UI, sin funcionalidad aún. Se agregó un GameManager. 
Se implementó un Quick Turn bruzco (pero hey, funciona) porque no quiero dedicarle mil horas a la camara en este momento. El sistema de movimiento actual puede volverse tosco al controlar el personaje porque el giro es algo abierto y el quick turn es la solución mas inmediata (accidentalmente terminé controlando el juego como si fuera un resident evil).

Actualización 6-  0.0.6 - 5/2/22 14:45
Agregadas actividades a modo de test de función. Todo este sistema va a ser refactorizado para usar Heredar, mas adelante. Estoy empezando a creer que lo mejor es seguir saltandome un poco de contenido porque madre mia, en que me meti.

Actualización 7 - 0.0.7 - 6/2/22 17:30
Agregado el depto del lvl 1. Agregadas colisiones. Falta: textura baño.

BUG: El character controller tiene muchisimos problemas con las colisiones de las esquinas. Considerando cambiarlo a un sistema de movimiento con fisicas.

Actualización 8 - 0.0.8 - 7/2/22 01:00
Agregados heladera, horno, sillas y mesa. Colisiones en: heladera, horno, mesa. Hay que ajustar los poligonos de la mesa. Existe un plano sobre el monoambiente para que la luz del sol no lo atraviese. 
Se agregaron luces de techo.

Bug: Las luces de techo atraviesan algunas paredes. Los UVs deben estar rotos. Las sombras a veces salen medio mal de los objetos.

Actualización 9 - 0.0.9 7/2/22 12:20 (Menor)
Borré algunos imports innecesarios y agregué el modelo de la tv plana vieja, con su collider. 

Actualización 10 - 0.0.10 7/3/22 18:45
Implementados layer de Actividad, Raycast para interactuar con las actividades. Estan funcionando perfectamente. Hecho!
Sin bugs nuevos observados.

Actualización de Orden - 7/2/22 21:30

Actualización 11 - 0.0.11 7/2/22 18:00
Agregados timers para Spawnear un objeto y que el objeto se autodespawnee. El timer no se resetea (ni debe resetearse) al obtener el objeto.
Esto se va a utilizar para spawnear objetos como para bono de velocidad temporal, que suba 10 stamina... etc.
Sin bugs nuevos observados.

Actualización 12 - Primer entrega.