## Out the Window - Proyecto DDV
### Historia
Una pareja de casados logró mudarse a un monoambiente para vivir su vida juntos, decidiendo que uno de los dos se quede para cuidar el hogar. Mientras que el otro trabaja, el jugador va a ser el amo de casa que se encargue de que la casa no se caiga abajo.

Para lograr su cometido de tener una casa mejor, acceso a un auto y electrodomésticos cada vez mas fiables, uno tiene que convertirse en un autentico amo de casa dragón, capaz de administrarse de manera que sobre dinero y le quede algo de espíritu al final del día. 

Mudarse a un nuevo hogar conlleva sus propias responsabilidades nuevas, pero el éxito de tener mayor espacio para hobbies es lo que permite al pequeño koi convertirse en ese dragón de los quehaceres que tanto busca ser, evitando tirar la casa por la ventana (o por lo menos, hasta tener los recursos para llenar el techo de manteca)

(Extraido de: Proyecto DDV - OTW)

### Controles
- Movimiento: WASD
- Sumar Stamina: Q
- Restar Stamina: Barra Espaciadora
- Interactuar: Enter
- Quickturn (Giro en 180): E

## Primera Entrega
- Inputs del jugador: HECHO
- Materiales para todos los objetos y texturas: Hecho (El cubo debajo de la tv y la parte de abajo del pino estan sin texturar a proposito, el cubo por temporal y el pino porque no se ve nunca)
- Luces puntuales: Hecho, al menos parcialmente. (Luces de techo)
- Prefabs: Si bien la intencion no es precisamente que pases de nivel con los muebles default... hecho.
- Temporizadores: Tiempo (sin implementar porque necesita un estado en especifico), timer de spawn de items, timer de despawneo de items.
- Sistema de camaras: La camara principal es Tercera Persona. La idea es que enfoque cuando hace la actividad, pero no estan implementadas formalmente, asi que se omitió esta porque lo unico que haria es hacer snap hacia la actividad y no salir. Hecho...?
- Calculos vectoriales: Movimiento, giro rapido.
- Switch: Sobran con todo el sistema de stamina y para definir que tipo de Actividad es.
- 3 tipos de colisiones distintas: Las actividades tienen su propio layer, pero despues de eso, todo el monoambiente y el personaje tienen sus colisiones.
- Animaciones de personaje: Hecho.

### Bugs
- Si se cambia el view de 1920x1080 se rompe la UI
- La barra de stamina no refleja bien el color, asi que se queda como degradado mientras.
- El personaje atraviesa algunas colisiones.
- Como los modelos los hice yo, las UV estan mal configuradas porque todavia no se configurar UVs.
 - Por eso, las texturas se buggean.
 - Tambien por eso, las luces no atraviesan del todo bien algunas cosas.
 
### Notas
Eventualmente voy a reemplazar las texturas por unas hechas a mano.
Eventualmente, voy a volver a importar los modelos con las UVs arregladas.

No aclaré esto, pero las actividades son como un "minijuego" en el que tenes que presionar unas teclas determinadas para que se hagan exitosamente. Por ahora, solo restan y suman a la barra de stamina.

El spawner de items es para spawnear items (je) que te suban la stamina o te hagan moverte mas rapido, por ejemplo. Lo que está puesto ahora es un test.

Los sistemas de dinero y recursos van para una entrega proxima porque no son "base" base, son "parte dos" base. Lo que importa es que el juego sea funcional en el estado en el que está, luego que las actividades se puedan "jugar", y de ahí partir con un sistema de recursos y dinero en el que puedas hacer mas rapido o mejor las cosas. Mismo caso con los estados de Éxtasis y Cansancio Extremo.

La onda es lograr un juego frenético tipo Overcooked con un sistema de "energia" tipo Stardew Valley. Todos los valores puestos pueden cambiar.
